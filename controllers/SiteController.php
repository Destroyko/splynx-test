<?php

namespace app\controllers;

use app\models\Customer;
use app\models\Services;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Request;



class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionServices()
    {
        $this->getView()->title = 'services';

        $requests = new Request();
        return $this->render('services', [
            'model' => new Services($requests->get('id', null))
        ]);
    }

    public function actionIndex()
    {

        $this->getView()->title = 'Splynx Add-on Skeleton';

        return $this->render('index', [
            'model' => new Customer()
        ]);
    }
}
