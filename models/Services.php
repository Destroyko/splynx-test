<?php

namespace app\models;

use splynx\helpers\ApiHelper;


class Services extends \splynx\models\Customer
{

    public static $baseApiCall = 'admin/customers/customer';

    public static $serviceInternet = 'internet-services';
    public static $serviceVoice = 'voice-services';
    public static $serviceCustom = 'custom-services';

    public static $customer = null;

    function __construct($id)
    {
        parent::__construct();
        return self::$customer = isset($id) ? $id : null;
    }

    public static function getApiCall($customer_id, $service_type)
    {
        $uri = self::$baseApiCall . '/' . $customer_id . '/' . $service_type;

        return $uri;
    }

    public static $services;

    private static function findInstanceByName ($instance_name)
    {
        $result = ApiHelper::getInstance()->get(self::getApiCall(self::$customer, $instance_name));

        if ($result['result'] == false or empty($result['response'])) {
            return [];
        }

        $models = [];
        foreach ($result['response'] as $row) {
            $models[] = $row;
        }

        return $models;
    }

    public static function getInternetServices()
    {
       return self::findInstanceByName(self::$serviceInternet);
    }

    public static function getVoiceServices()
    {
        return self::findInstanceByName(self::$serviceVoice);
    }

    public static function getCustomServices()
    {
        return self::findInstanceByName(self::$serviceCustom);
    }

}
