<?php

// Check to enable development mode
if (file_exists(__DIR__ . '/../config/dev.php')) {
    require(__DIR__ . '/../config/dev.php');
}

// Load Splynx Base Add-on vendor
require(__DIR__ . '/../../splynx-addon-base/vendor/autoload.php');
require(__DIR__ . '/../../splynx-addon-base/vendor/yiisoft/yii2/Yii.php');

// Load add-on vendor
require(__DIR__ . '/../vendor/autoload.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
